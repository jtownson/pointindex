package co.sitetour.codetest.pointindex

case class Point(id: Int, x: Int, y: Int) {
  def ordinate(i: Int) = if (i == 0) x else y
}
