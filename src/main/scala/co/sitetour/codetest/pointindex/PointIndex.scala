package co.sitetour.codetest.pointindex

class PointIndex(points: Traversable[Point]) {

  val tree = new Tree2D(points.toSeq)

  /**
   * Return the `take` closest points to `(x, y)` in this index.
   */
  def closestTo(x: Int, y: Int, take: Int): Seq[Point] = tree.findNearest(Point(0, x, y), take)

}
