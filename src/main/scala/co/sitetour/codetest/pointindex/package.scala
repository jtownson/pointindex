package co.sitetour.codetest

import scala.math.sqrt

package object pointindex {

  /**
   * Calculate the Euclidean distance between `p` and `q`. The Euclidean
   * distance between points p and q is the length of the line segment
   * connecting them.
   *
   * @param p Point P (x, y).
   * @param q Point Q (x, y).
   */
  def euclideanDistance(p: (Int, Int), q: (Int, Int)): Double = {
    sqrt(sqr(q._1 - p._1) + sqr(q._2 - p._2))
  }

  def compareBy(axis: Int): Ordering[Point] = Ordering.by{ p => (p.ordinate(axis), p.id)}

  def sqr(v: Int): Long = v.toLong * v.toLong

}
