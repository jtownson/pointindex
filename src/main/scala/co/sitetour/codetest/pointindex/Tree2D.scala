package co.sitetour.codetest.pointindex

import scala.collection.mutable

/**
 * The simplest possible implementation of a 2D tree.
 * For test cases, see PointIndexSpec.
 */
class Tree2D(points: Seq[Point]) {

  val root = buildTree(0, points)

  sealed trait Node {
    def size(): Int

    def empty(): Boolean

    def contains(p: Point): Boolean

    def get(p: Point): Option[Node]
  }

  case class INode(location: Point, axis: Int, left: Node, right: Node) extends Node {

    override def size(): Int = left.size + right.size + 1

    override def empty(): Boolean = size == 0

    override def contains(p: Point): Boolean = get(p).isDefined

    override def get(p: Point): Option[Node] = {
      if (p == location) {
        Some(this)
      } else {
        if (p.ordinate(axis) < location.ordinate(axis))
          left.get(p)
        else
          right.get(p)
      }
    }
  }

  case object EmptyNode extends Node {

    override def size(): Int = 0

    override def get(p: Point): Option[Node] = None

    override def empty(): Boolean = true

    override def contains(p: Point): Boolean = false
  }

  private def buildTree(depth: Int, points: Seq[Point]): Node = {

    def findSplit(points: Seq[Point], axis: Int): (Point, Seq[Point], Seq[Point]) = {

      val sp = points.sorted(compareBy(axis))

      val medianIndex = sp.length / 2

      (sp(medianIndex), sp.take(medianIndex), sp.drop(medianIndex + 1))
    }

    if (points.isEmpty) {
      EmptyNode
    } else {
      val axis = depth % 2
      val (location, left, right) = findSplit(points, axis)
      new INode(location, axis, buildTree(depth + 1, left), buildTree(depth + 1, right))
    }
  }


  /**
   * N nearest neighbour finding using a bounded priority queue approach
   * (described in http://web.stanford.edu/class/cs106l/handouts/assignment-3-kdtree.pdf)
   *
   * This is a fairly imperative approach because the BPQ class I had to hand is mutable
   * (taken from apache stream).
   *
   * The problem with this approach is the possibility of stack overflows for very large trees.
   * The benefit is simplicity.
   *
   * @param p the test point
   * @param n the number of neighbours to find
   * @return the nearest neighbours
   */
  def findNearest(p: Point, n: Int): Seq[Point] = {

    val neighbours = BoundedPriorityQueue[(Point, Double)](n)(Ordering.by{ nodeScore => (nodeScore._2, nodeScore._1.id) })

    def loop(node: Node): mutable.PriorityQueue[(Point, Double)] = node match {

      case EmptyNode =>
        neighbours
      case in: INode => {

        val p0 = (p.x, p.y)
        val pn = (in.location.x, in.location.y)
        val distance = euclideanDistance(p0, pn)

        neighbours.enqueue((in.location, distance))

        val searchNodes: (Node, Node) =
          if (p.ordinate(in.axis) < in.location.ordinate(in.axis)) (in.left, in.right) else (in.right, in.left)

        loop(searchNodes._1)

        val maxEuclidianDistance = neighbours.head._2

        val axialDistance = Math.abs(p.ordinate(in.axis) - in.location.ordinate(in.axis))

        if (neighbours.size < n || axialDistance < maxEuclidianDistance) {
          loop(searchNodes._2)
        }

        neighbours
      }
    }

    loop(root)

    neighbours.dequeueAll.reverse.map(_._1)
  }

}

