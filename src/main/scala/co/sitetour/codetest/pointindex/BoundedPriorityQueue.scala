package co.sitetour.codetest.pointindex

import scala.collection.mutable

/**
 * Bounded priority queue impl.
 * Taken from apache stream.
 */
trait BoundedPriorityQueue[A] extends mutable.PriorityQueue[A] {

  def maxSize: Int

  override def +=(a: A): this.type = {
    if (size < maxSize) super.+=(a)
    else maybeReplaceLowest(a)
    this
  }

  override def ++=(xs: TraversableOnce[A]): this.type = {
    xs.foreach { this += _ }
    this
  }

  override def +=(elem1: A, elem2: A, elems: A*): this.type = {
    this += elem1 += elem2 ++= elems
  }

  override def enqueue(elems: A*) {
    this ++= elems
  }

  private def maybeReplaceLowest(a: A) {
    // note: we use lt instead of gt here because the original
    // ordering used by this trait is reversed
    if (ord.lt(a, head)) {
      dequeue()
      super.+=(a)
    }
  }
}

object BoundedPriorityQueue {
  def apply[A: Ordering](maximum: Int): BoundedPriorityQueue[A] = {
    // note: reverse the ordering here because the mutable.PriorityQueue
    // class uses the highest element for its head/dequeue operations.
    //val ordering = implicitly[Ordering[A]].reverse
    new mutable.PriorityQueue[A]()/*(ordering)*/ with BoundedPriorityQueue[A] {
      //implicit override val ord = ordering
      override val maxSize = maximum
    }
  }
}