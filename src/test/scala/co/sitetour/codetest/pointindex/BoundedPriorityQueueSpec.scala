package co.sitetour.codetest.pointindex


class BoundedPriorityQueueSpec extends BaseSpec {

  val p0 = Point(0, 0, 0)
  val p1 = Point(1, 1, 1)
  val p2 = Point(2, 2, 2)
  val p3 = Point(3, 9, 9)

  def fixture = new {
    val bpq = BoundedPriorityQueue[(Point, Double)](3)(Ordering.by(_._2))

    bpq += (p0 -> 3.0)
    bpq += (p1 -> 0.2)
    bpq += (p2 -> 0.5)
    bpq += (p3 -> 10.0)
  }

  "Given points and distances" should "enqueue the lowest distances" in {

    fixture.bpq.dequeueAll.reverse shouldBe Seq(p1 -> 0.2, p2 -> 0.5, p0 -> 3.0)
  }

  "Given points and distances" should "peek the max priority element" in {

    fixture.bpq.head shouldBe p0 -> 3.0
    fixture.bpq.size shouldBe 3
  }
}
