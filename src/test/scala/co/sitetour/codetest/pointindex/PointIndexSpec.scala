package co.sitetour.codetest.pointindex

import java.io.PrintWriter

import scala.util.Random

class PointIndexSpec extends BaseSpec {

  "Given a small sample" should "find the closest points" in {

    // given
    val p0 = Point(id=0, 3, 5)
    val p1 = Point(id=1, 9, 4)
    val p2 = Point(id=2, 17, 6)
    val p3 = Point(id=3, 18, 7)

    val points: Seq[Point] = Seq(p2, p0, p3, p1)

    // when
    val index = new PointIndex(points)
    val neighbours: Seq[Point] = index.closestTo(0, 5, take=4)

    // then
    neighbours shouldBe Seq(p0, p1, p2, p3)
  }

  "Given an extreme sample" should "find the closest points" in {

    // given
    val p0 = Point(id=0, Int.MaxValue, Int.MaxValue)
    val p1 = Point(id=1, Int.MaxValue, Int.MinValue)
    val p2 = Point(id=2, Int.MinValue, Int.MaxValue)
    val p3 = Point(id=3, Int.MinValue, Int.MinValue)

    val points: Seq[Point] = Seq(p2, p0, p3, p1)

    // when
    val index = new PointIndex(points)
    val neighbours: Seq[Point] = index.closestTo(1, 1, take=4)

    // then
    neighbours shouldBe Seq(p0, p1, p2, p3)
  }

  "Given equal points" should "use id field to discriminate them" in {

    // given
    val p0 = Point(id=0, 1,	1)
    val p1 = Point(id=1, 1,	1)
    val p2 = Point(id=2, 1,	1)

    val points: Seq[Point] = Seq(p0, p1, p2)

    // when
    val index = new PointIndex(points)
    val neighbours = index.closestTo(0, 0, 3)

    // then
    neighbours should be(Seq(p0, p1, p2))
  }

  "Given large ints that overflow" should "match brute force result" in {

    // given
    val p0 = Point(0, 349289,	144359)
    val p1 = Point(1, 243236,	329013)
    val p2 = Point(2, 555355,	168136)
    val p3 = Point(3, 681690,	765119)
    val p4 = Point(4, 37376,	517943)
    val p5 = Point(5, 738003,	554880)
    val p6 = Point(6, 764929,	820141)
    val p7 = Point(7, 785903,	206134)

    val points: Seq[Point] = Seq(p0, p1, p2, p3, p4, p5, p6, p7)
    val take = 3
    val (x, y) = (384000, 275192)

    // when
    val index = new PointIndex(points)
    val neighbours = index.closestTo(x, y, take)

    neighbours shouldBe bruteForceClosestTo(points, x, y, take)
  }

  "Given points on a line" should "work" in {

    // given
    val p0 = Point(0, 100000,	100000)
    val p1 = Point(1, 200000,	200000)
    val p2 = Point(2, 300000,	300000)
    val p3 = Point(3, 400000,	400000)
    val p4 = Point(4, 500000,	500000)
    val p5 = Point(5, 600000,	600000)
    val p6 = Point(6, 700000,	700000)
    val p7 = Point(7, 800000,	800000)

    val points: Seq[Point] = Seq(p0, p1, p2, p3, p4, p5, p6, p7)
    val take = 10
    val (x, y) = (0, 0)

    // when
    val index = new PointIndex(points)
    val neighbours = index.closestTo(x, y, take)

    neighbours shouldBe bruteForceClosestTo(points, x, y, take)
  }

  "PointIndex" should "find the closest n points to a provided point." in {
    val (n, w, h) = (Math.pow(10,5).toInt, Math.pow(10,6).toInt, Math.pow(10,6).toInt)
    val points = makePoints(n, w, h)
    val index = new PointIndex(points)

    val rnd = new Random()
    val t = 3
    val (x, y) = rnd.nextInt(w) -> rnd.nextInt(h)

    val bruteForceCloseTo = bruteForceClosestTo(points, x, y, t)

    val kdCloseTo = index.closestTo(x, y, t)

    kdCloseTo should be(bruteForceCloseTo)
  }

  ignore should "print some time stats" in {

    val rnd = new Random()
    val t = 3
    val buildWriter: PrintWriter = new PrintWriter("build-times.csv")
    val queryWriter: PrintWriter = new PrintWriter("query-times.csv")

    (0 to 100000 by 1000).foreach{ n =>
      val (w, h) = (Math.pow(10,6).toInt, Math.pow(10,6).toInt)
      val points = makePoints(n, w, h)
      val logn = if (n == 0) 0 else Math.log(n)

      val buildStart = System.currentTimeMillis
      val index = new PointIndex(points)
      val buildEnd = System.currentTimeMillis
      buildWriter.write(s"$n,${buildEnd-buildStart},${n*logn}\n")
      val (x, y) = rnd.nextInt(w) -> rnd.nextInt(h)

      val queryStart = System.currentTimeMillis
      index.closestTo(x, y, t)
      val queryEnd = System.currentTimeMillis


      queryWriter.write(s"$n,${queryEnd-queryStart},$logn\n")
    }
    queryWriter.close()
    buildWriter.close()

  }

  // --------------------------------------------------------------------------

  /** Brute force nearest neighbour. For testing. */
  private def bruteForceClosestTo(points: Seq[Point], x: Int, y: Int, take: Int): Traversable[Point] = {
    val p = (x, y)
    points.sortBy(r => euclideanDistance(p, r.x->r.y)).take(take)
  }

  private def makePoints(n: Int, w: Int, h: Int) = {
    val rnd = new Random()
    (1 to n) map(i => Point(i, rnd.nextInt(w), rnd.nextInt(h)))
  }

}
