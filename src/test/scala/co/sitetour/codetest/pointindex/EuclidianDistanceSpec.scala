package co.sitetour.codetest.pointindex

class EuclidianDistanceSpec extends BaseSpec {

  "Euclidian distance" should "work for equal points" in {

    euclideanDistance(300000->300000, 300000->300000) shouldBe 0
  }

  "Euclidian distance" should "work for far away points" in {

    Math.ceil(euclideanDistance(0->0, 300000->300000)) shouldBe 424265
  }

  "Square of a large number" should "not overflow" in {

    sqr(300000) shouldBe 90000000000L
  }
}
