# Geometric Index Problem

## Problem.

This is a search and indexing problem to find the closest points from some set to any
given point. A finite set of points _P_ is given. We provide an implementation
of PointIndex such that it returns the m closest points in P for any given point, in order, with the closest first.

Given a set of points P in R^2 and a point q in R^2, the point p in P that minimises the function `sqrt((p.x - q.x)^2 + (p.y - q.y)^2)`, over all points in P, is said to be the point in P which is _closest_ to q. The `Point` type has an id property, which is to be used to break equidistant ties (where the lowest id wins).

## Solution


## Implementation notes

The solution I chose was to build a 2D-Tree from the points.
Nearest neighbour algorithms are then described in http://web.stanford.edu/class/cs106l/handouts/assignment-3-kdtree.pdf
and http://www.programmingusingscala.net/ (plus a bunch of other youtube videos).

The Tree implementation is my own. It falls back to the scala library for

- binary sorting of the input points (in the x and y axes respectively)
- the priority queue used to store the N nearest neighbours of a point (there is bounded priority
queue extension, which I took from apache stream).

The complexity of K-D trees is documented in, for example, https://en.wikipedia.org/wiki/K-d_tree#Complexity. 
Typically a tree can be built in n.log(n) and queried for N neighbours in ~log(n) time (assuming the take/N value is small
compared to the number of points).
The `"PointIndex" should "print some time stats"` test case shows that for randomly generated data, the index build time is linear and, for small take values, the query time is constant.  

I did consider the following geo-hashing approach. Seemingly, many geohash algorithms are approximate, in that they map a 2-D coordinate to small cell in the plane and within each cell, the sort order for nearest neighbours 
could vary with varying test points.

For an exact solution, one needs a grid construction for which the sort order is constant within each cell. Quoting this discussion on stackexchange (http://cstheory.stackexchange.com/questions/8717/sorting-by-euclidean-distance).

> Find the n^2 perpendicular bisectors between pairs of points, and construct the arrangement of these lines.
> The arrangement has n^4 cells, within which the sorted order is constant.
  
This would suggest that for n (2D) input points, assigning (1D) hashes to each of the cells defined above would allow you to adapt a normal 1D search tree to this 2D problem and achieve an exact nearest neighbour search.

In the end, I chose the 2D-tree approach because it seemed a little simpler (and safer).